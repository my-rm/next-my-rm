import MainHeader from "@/components/header/main-header";
import "./globals.css";

export const metadata = {
  title: "my-rm",
  description:
    "This is Ramin Maghsoodi visual designer, Web Developer, Creative",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>
        <MainHeader />
        {children}
      </body>
    </html>
  );
}
