export default function SlugMeals({ params }) {
  return (
    <main>
      <h1 style={{ color: "white", textAlign: "center" }}>
        SLUG-MEALS : {params.slug_meals}
      </h1>
    </main>
  );
}
