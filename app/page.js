import classes from "./page.module.css";

export default function Home() {
  return (
    <>
      <header className={classes.header}></header>
      <main>
        <section className={classes.section}></section>
        <section className={classes.section}></section>
      </main>
    </>
  );
}
