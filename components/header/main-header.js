"use client";
import Link from "next/link";
import Logo from "@/assets/RM.webp";
import classess from "./main-header.module.css";
import Image from "next/image";
import NavLink from "./nav-link";

export default function MainHeader() {
  return (
    <header className={classess.header}>
      <Link href="/" className={classess.logo}>
        <Image src={Logo} alt="ًRamin Maghsoudi" priority />
      </Link>
      <nav className={classess.nav}>
        <ul>
          <li>
            <NavLink href="/home">home</NavLink>
          </li>
          <li>
            <NavLink href="/about">about</NavLink>
          </li>
          <li>
            <NavLink href="/service">service</NavLink>
          </li>
          <li>
            <NavLink href="/portfolio">portfolio</NavLink>
          </li>
          <li>
            <NavLink href="/resume">resume</NavLink>
          </li>
          <li>
            <NavLink href="/contact">contact</NavLink>
          </li>
        </ul>
      </nav>
      <span>465</span>
    </header>
  );
}
